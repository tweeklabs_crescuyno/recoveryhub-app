import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Subject, Observable } from 'rxjs/Rx';
import 'rxjs/Rx';

@Injectable()
export class PingService {

  pingStream: Subject<number> = new Subject<number>();
  ping: number = 0;
  url: string = "https://cors-test.appspot.com/test";
  //url: string = "'http://boticajohn-admin.dev/api/ping";
  
  constructor(private _http: HttpClient) {
    Observable.interval(2000)
      .subscribe((data) => {
        let timeStart: number = performance.now();
        
        this._http.get(this.url)
          .subscribe((data) => {
            let timeEnd: number = performance.now();
            
            let ping: number = timeEnd - timeStart;
            this.ping = ping;
            this.pingStream.next(ping);
          },
          err => {
            this.ping = 0;
            this.pingStream.next(this.ping);
          });
      },(err) => { console.log( err)});
  }

}
