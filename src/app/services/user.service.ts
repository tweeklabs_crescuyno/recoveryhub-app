import { Injectable } from '@angular/core';
// import { Http, Headers, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
// import { Observable } from 'rxjs';
// import 'rxjs/add/operator/map';

import { AbstractService } from './AbstractService';

import { environment } from './../../environments/environment';

@Injectable()
export class UserService extends AbstractService{

    constructor(http: HttpClient){
        super(http);
        this.endpoint = environment.apiUrl + 'users/';
    }

    getList(e?) {
        return this.index(e);
    }

    clearCurrentUser() {
        localStorage.removeItem('currentUser');
    }

    setCurrentUser(user) {
        localStorage.setItem('currentUser', user);
    }

    getCurrentUser() {
        return localStorage.getItem('currentUser');
    }

    _getCurrentUser() {
        console.log(environment.apiUrl + 'auth/token')
        return this.http.get(environment.apiUrl + 'auth/token')
            .map((response: any) => {
                console.log(response);
                // login successful if there's a jwt token in the response
                if (response.user) {

                    

                    return true;
                } else {
                    console.log('FAILED');
                    // return false to indicate failed login
                    return false;
                }
            });
    }

    register(data) {
        console.log(data);
        return this.http.post(environment.apiUrl + 'users/register', data)
            .map((response:any) => {
                console.log(response)
                return response;
            })
    }
}