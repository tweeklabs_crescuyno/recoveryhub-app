import { Injectable } from '@angular/core';
//import { Http, Headers, Response } from '@angular/http';
import { HttpClient, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

import { environment } from './../../environments/environment';

import { UserService } from './';

// Nebular

@Injectable()
export class AuthenticationService {

    public token: string;
    private tokenKey: string = 'app_token';
    
    constructor(private http: HttpClient) {
        //super()
        // set token if saved in local storage
        //var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        //this.token = currentUser && currentUser.token;
    }

    setToken(token: string): void {
        localStorage.setItem(this.tokenKey, token);
    }

    authenticated() {
        return this.http.get(environment.apiUrl + 'auth/token')
        .map(response => {
            return response['user']
        });
    }

    authenticate(data) {
        return this.http.post(environment.apiUrl + 'auth', JSON.stringify({ email: data.email, password: data.password }))
        .map((response: any) => {
            console.log(response)
            // login successful if there's a jwt token in the response
            if (response.token) {
                
                // set token property
                this.setToken(response.token);

                this.authenticated().subscribe(
                    (user) => {
                        localStorage.setItem('current_user', JSON.stringify(user));
                    }
                )

                // store username and jwt token in local storage 
                // to keep user logged in between page refreshes
                //localStorage.setItem('currentUser', JSON.stringify({ username: username, token: token }));
                
                // return true to indicate successful login
 
                //response;
            } else {
                console.log('FAILED');
                // return false to indicate failed login

            }
        });
    }

    logout() {
        localStorage.removeItem(this.tokenKey);
        return;
    }

    register(data) {
        return ;
    }

    requestPassword(data) {
        return ;
    }

    resetPassword(data) {
        return ;
    }
}

export class AuthenticationServicea {
    public token: string;
    private tokenKey: string = 'app_token';

    constructor(private http: HttpClient) {

        // set token if saved in local storage
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;
    }

    cachedRequests: Array<HttpRequest<any>> = [];

    public collectFailedRequest(request): void {
        this.cachedRequests.push(request);
      }

    setConfig(config) {
        console.log('CONFIG: ' , config)

    }

    authenticate(data)  {

        let username = data.email;
        let password = data.password;

        return this.http.post(environment.apiUrl + 'auth', JSON.stringify({ email: username, password: password }))
            .map((response: any) => {

                // login successful if there's a jwt token in the response
                if (response.token) {

                    // set token property
                    this.setToken(response.token);

                    // store username and jwt token in local storage 
                    // to keep user logged in between page refreshes
                    //localStorage.setItem('currentUser', JSON.stringify({ username: username, token: token }));
                    
                    // return true to indicate successful login
                    return response;
                } else {
                    console.log('FAILED');
                    // return false to indicate failed login
                    return response;
                }
            });
    }

    setToken(token: string): void {
        localStorage.setItem(this.tokenKey, token);
    }

    getToken() {
        return localStorage.getItem(this.tokenKey)
    }

    generateNewToken() {
        return this.http.get(environment.apiUrl + 'auth');
    }


    isAuthenticated() {
        return localStorage.getItem(this.tokenKey) ? true : false;
    }
    
    logout(): void {
        localStorage.removeItem(this.tokenKey);
    }
}