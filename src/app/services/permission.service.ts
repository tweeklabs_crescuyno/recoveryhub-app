import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AbstractService } from './AbstractService';

import { environment } from './../../environments/environment';
@Injectable()
export class PermissionService extends AbstractService{

    constructor(http: HttpClient){
        super(http);
        this.endpoint = environment.apiUrl + 'permissions/';
    }
}
