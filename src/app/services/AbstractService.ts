export abstract class AbstractService implements ServiceInterface {

    endpoint = '';

    constructor(public http) {}

    index(e?) {
               // {
        //     count: 24, 
        //     pageSize: 15, 
        //     limit: 15, 
        //     offset: 0


        // take
        // page
        // }
        let sq = '';
        if (e) {
            if (e.offset) {
                sq += '&page=' + (e.offset + 1);
            } else if (e.pageSize) {
                sq += '&take=' + e.pageSize;
            } else if (typeof(e) == 'string') {

                sq += '&' + e
            }
        }

        return this.http.get(this.endpoint + '?' + sq)
            .map(result => {
                return result;
            });
    }
    
    show(id) {
        return this.http.get(this.endpoint + id + '?include=roles')
        .map(result => {
            return result;
        }); 
    }

    destroy(id) {
        return this.http.delete(this.endpoint + id)
        .map(result=> {
            return result;
        })
    }
    
    update(id, params){
        return this.http.put(this.endpoint + id, params)
            .map(result => {
                return result;
            }, err => {
                return err;
            })

    }

    store(data) {
        return this.http.post(this.endpoint, data)
            .map(result => {
                return result;
            }, err => {
                return err;
            })
    }


    setEndpoint(endpoint) {
        this.endpoint = endpoint;
    }
}


export interface ServiceInterface {
    endpoint: String;

    index: Function;
    show: Function;
    destroy: Function;
    update: Function;
    store: Function;
}