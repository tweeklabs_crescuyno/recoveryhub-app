import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';

import { ThemeModule } from './../theme/theme.module'
import { HeaderModule } from './../theme/header/header.module';
import { FooterModule } from './../theme/footer/footer.module';

import { SingleComponent } from './../theme/layouts/single/single.component';
import { NoContentComponent } from './no-content';

@NgModule({
  imports: [
    CommonModule,
    ThemeModule,
    HeaderModule,
    FooterModule,
    PagesRoutingModule
  ],
  declarations: [
    PagesComponent,
    NoContentComponent,
    //SingleComponent
  ],
  exports: [
    PagesComponent
  ]
})
export class PagesModule { }
