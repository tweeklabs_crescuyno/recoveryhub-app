import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// ADD THIS BECAUSE THIS COMPONENT USES BOOTSTRAP ALERT
import { AlertModule } from 'ngx-bootstrap/alert';

// ADD THIS BECAUSE THIS COMPONENT IS USING FORMS THEN ADD TO IMPORTS
import { ReactiveFormsModule } from "@angular/forms";

import { LoginComponent } from './login.component';
import { LoginRoutingModule } from './login-routing.module';
import { AuthenticationService } from './../../services';

@NgModule({
  imports: [
    CommonModule,
    LoginRoutingModule,
    ReactiveFormsModule,
    AlertModule.forRoot(),
  ],
  declarations: [
    LoginComponent,
  ],

  providers: [
    AuthenticationService
  ],
})
export class LoginModule { }
