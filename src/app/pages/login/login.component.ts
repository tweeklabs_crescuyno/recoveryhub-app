import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

import { LOGIN_MODEL } from './login.model';

import { AuthenticationService } from './../../services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  loginFailed: Boolean = false;
  authenticating: Boolean = false;

  constructor(
    private AuthService: AuthenticationService,
    fb: FormBuilder
  ) {
    this.form = fb.group(LOGIN_MODEL);
  }

  ngOnInit() {
    //this.form.
  }

  onSubmit() {
    this.loginFailed = false;
    this.authenticating = true;
    this.AuthService.authenticate(this.form.value)
      .subscribe(result => {
        //console.log(result)
        this.authenticating = false;
      },
      err => {
        this.loginFailed = true;
        this.authenticating = false;
        console.log(this.loginFailed)
        console.log(err)
      })
  }
}
