import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { LoginComponent } from './login.component';

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                component: LoginComponent,
            }
        ]
        
        // children: [      
        //     { path: '**',    component: NoContentComponent },
        // ],  
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoginRoutingModule {
}