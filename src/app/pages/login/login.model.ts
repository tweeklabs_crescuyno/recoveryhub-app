import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';


export const LOGIN_MODEL = {
    'email'     : ['', Validators.required],
    'password'  : ['', Validators.required]
}