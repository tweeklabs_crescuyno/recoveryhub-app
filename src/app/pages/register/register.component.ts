import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

import { STEP1_MODEL, STEP2_MODEL, STEP3_MODEL } from './register.model';

import { UserService } from './../../services';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  formStep1: FormGroup;
  formStep2: FormGroup;
  formStep3: FormGroup;

  showStep1: Boolean = true;
  showStep2: Boolean = false;
  showStep3: Boolean = false;
  
  data: Object = {};

  constructor(
    private userService: UserService,
    fb: FormBuilder
  ) { 
    this.formStep1 = fb.group(STEP1_MODEL);
    this.formStep2 = fb.group(STEP2_MODEL);
    this.formStep3 = fb.group(STEP3_MODEL);

  }

  ngOnInit() {
  }

  validateStep1() {
    console.log(this.formStep1);

    if (this.formStep1.valid) {
      Object.assign(this.data, this.formStep1.value);
      console.log(this.data);
      this.showStep2 = true;
      this.showStep1 = false;
    }
  }

  validateStep2() {
    if (this.formStep2.valid) {
      Object.assign(this.data, this.formStep2.value);
      console.log(this.data);
      this.showStep3 = true;
      this.showStep2 = false;
    }
  }
  validateStep3() {
    if (this.formStep3.valid) {
      Object.assign(this.data, this.formStep3.value);
      console.log(this.data);
      this.userService.register(this.data)
        .subscribe(response => {
            console.log(response)
          },
          err => {
            console.log(err);
          });
      console.log('start post')
    }


  }

}
