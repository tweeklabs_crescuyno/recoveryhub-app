import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// HANDLING FORM ERRORS
import { NgxErrorsModule } from '@ultimate/ngxerrors';


// ADD THIS BECAUSE THIS COMPONENT IS USING FORMS THEN ADD TO IMPORTS
import { ReactiveFormsModule } from "@angular/forms";

// ADD THIS BECAUSE THIS COMPONENT USES BOOTSTRAP ALERT
import { AlertModule } from 'ngx-bootstrap/alert';

// IMPORT ANY SERVICES NEEDED BY THE COMPONENT AND ADD TO PROVIDERS
import { UserService } from './../../services';


import { RegisterRoutingModule } from './register-routing.module';
import { RegisterComponent } from './register.component';



@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AlertModule,
    NgxErrorsModule,
    RegisterRoutingModule
  ],
  declarations: [
    RegisterComponent
  ],
  exports: [],

  providers: [
    UserService,
  ]
})
export class RegisterModule { }
