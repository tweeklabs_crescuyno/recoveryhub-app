import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';


export const STEP1_MODEL = {
    'email'     : ['', [Validators.required, Validators.email]],
    'password'  : ['', Validators.required],
    'confirm_password'  : ['', Validators.required]
}

//First name, 
//Middle name, Last name, Nick name, Birth date, Birth place, Contact no., Address, Occupation, Hobbies and Interest, Favorite Quote 
export const STEP2_MODEL = {
    'first_name': ['', [Validators.required]],
    'middle_name': ['', Validators.required],
    'last_name': ['', Validators.required],
    // 'alias': ['', Validators.required],
    // 'birthdate': ['', Validators.required],
    // 'birthplace': ['', Validators.required],
    // 'contact_no': ['', Validators.required],
    // 'address': ['', Validators.required],
    // 'occupation': ['', Validators.required],
    // 'hobbies': ['', Validators.required],
    // 'favorite_quote': ['', Validators.required],
}

export const STEP3_MODEL = {
    'agree': ['', Validators.required],
}