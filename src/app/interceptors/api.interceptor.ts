import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { 
  HttpInterceptor, 
  HttpHandler, 
  HttpRequest, 
  HttpEvent, 
  HttpResponse, 
  HttpErrorResponse 
} from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';

import { environment } from './../../environments/environment';
import { AuthenticationService } from './../services';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
  private authService: AuthenticationService;
  private router: Router;

  constructor(injector:Injector) {
    setTimeout(() => {
      this.authService = injector.get(AuthenticationService)
      this.router = injector.get(Router)
    });
  }


  intercept (req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let authReq = this.authenticateRequest(req);
    
    return next.handle(authReq)
    .do(
      evt => {
        //console.log(evt)
      },
      (error: any) => {
        if (error instanceof HttpErrorResponse) {
          if (error.status == 401 || error.status == 403) {
            this.router.navigate(['/login']);
          }
        }

      },
    );
  }

  authenticateRequest(req) {
    var headers = req.headers.set('Accept', environment.apiAcceptHeader);

    //if (this.authService.isAuthenticated()) {
    if (localStorage.getItem('app_token')) {
      headers = headers.set('Authorization', 'Bearer ' + localStorage.getItem('app_token'));
    }

    const authReq = req.clone({
      headers: headers.set('Content-type', 'application/json')
    });
    return authReq;

  }
}