import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpResponse, HttpErrorResponse }
  from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';


@Injectable()
export class HttpStatusInterceptor implements HttpInterceptor {
  private router;
  constructor(injector: Injector) {
    this.router = injector.get(Router)
  }

  intercept (req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).do(evt => {
      console.log(req)
      console.log(next)
      console.log(evt, evt instanceof HttpResponse)
      if (evt instanceof HttpResponse) {
        console.log('---> status:', evt.status);
        console.log('---> filter:', req.params.get('filter'));
      }
    }, (error: any) => {
      if (error instanceof HttpErrorResponse) {
        if (error.status == 401) {
          console.log('redi')
          this.router.navigate(['login']);
          return Observable.throw(next);
          //return next.handle(req);
        } else {
          return Observable.throw(error);
        }
        
      }
    });
      
  }
}