import { NgModule } from '@angular/core';
import { ExtraOptions, RouterModule, Routes, PreloadAllModules } from '@angular/router';
// import { HomeComponent } from './home';
// import { AboutComponent } from './about';
// import { NoContentComponent } from './no-content';

import { DataResolver } from './app.resolver';

const ROUTES: Routes = [
  //{ path: '',      component: HomeComponent },
  //{ path: 'home',  component: HomeComponent },
  //{ path: 'about', component: AboutComponent },
  // { path: 'detail', loadChildren: './+detail#DetailModule'},
  // { path: 'barrel', loadChildren: './+barrel#BarrelModule'},
  { path: '',      loadChildren: './pages/pages.module#PagesModule'},
  //{ path: '**',    component: NoContentComponent },
];

const config: ExtraOptions = {
  useHash: Boolean(history.pushState) === false,
  //preloadingStrategy: PreloadAllModules
  //enableTracing: true
};
@NgModule({
  imports: [RouterModule.forRoot(ROUTES,config)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}