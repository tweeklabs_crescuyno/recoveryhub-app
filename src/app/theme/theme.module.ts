import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HeaderModule } from './header/header.module';
import { HeaderComponent } from './header/header.component';
import { FooterModule } from './footer/footer.module';

import { SingleModule } from './layouts/single/single.module';
import { SingleComponent } from './layouts';


@NgModule({
  imports: [
    CommonModule,
    HeaderModule,
    FooterModule,
    SingleModule
  ],
  declarations: [
    SingleComponent,
  ],
  exports:[
    SingleComponent
  ]
})
export class ThemeModule { }
